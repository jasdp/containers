var expect = require('chai').expect;

var request = require('request');
var $ = require('chai-jquery');

describe("Pruebas sencillas", function() {
   it('Test suma', function() {
     expect(9+4).to.equal(13);
    });
});

describe("Pruebas red", function() {
  it('Test internet', function(done) {
    request.get("http://www.forocoches.com",
              function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
    });
  });

  it('Test local', function(done) {
    request.get("http://localhost:8081",
              function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
    });
  });

  it('Test body', function(done) {
    request.get("http://localhost:8081",
              function(error, response, body) {
                expect(body).contains('blog');
                done();
    });
  });
});

describe('Test contenido HTML', function() {
  it('Test H1', function(done) {
    request.get("http://localhost:8081",
              function(error, response, body) {
                console.log(body);
                expect($('body h1')).to.have.text("Bienvenido a mi blog");
                done();
    });
  });
});
