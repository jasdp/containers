//"use strict";

/*jslint browser: true*/
/*global $, jQuery, alert, describe, use, require, it, console*/
var chai = require('chai');

var expect = chai.expect;
var jsdom = require('jsdom');

var request = require('request');

describe("pruebas html", function () {
    "use strict";
    it('Test body', function (done) {
        request.get("http://localhost:8081",
            function (error, response, body) {
                //console.log(body);

                expect(body).contains('blog');
                done();
            });
    });
});


describe('Test contenido HTML', function () {

    it('Test body', function (done) {
        request.get("http://localhost:8081",
            function (error, response, body) {

                global.document = jsdom.jsdom(body);
                
                //   global.window = global.document.defaultView;
                // global.navigator = global.window.navigator;
                //   console.log(global.document);

                //console.log(document.getElementById('titul').innerHTML);

                expect(document.getElementById('titul').innerHTML).to.equal('Bienvenido a mi blog v3');
                
                done();
            });
    });

});
