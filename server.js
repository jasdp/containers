'use strict';

const express = require('express');
const path = require('path');

// Constantes
const PORT = 8081;

// App
const app = express();

app.use(express.static(__dirname));

app.get('/', function(req, res) {
  //res.send("Bienvenido terrícola\n");
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/detallePost/:id', function(req, res) {


  //res.sendFile(path.join(__dirname+'/detallePost.html?id='+req.params.id));
    var url = path.join(__dirname+'/detallePost.html');
    console.log("Enviando a buscar detalles a la siguiente url:"+url);
    res.set('oId',req.params.id);
    res.sendFile(url);




});
app.get('/admin', function(req, res) {

  res.sendFile(path.join(__dirname+'/admin.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto" + PORT);

